const assert = require('assert');

assert.deepStrictEqual(
    sumOfNumbers([1, 2, '3']),
    3,
    'You should not count string elements'
);
